﻿namespace Plugins.LookupService
{
    /// <summary>
    /// Lookupstrategy that uses an internal switch for lookups
    /// </summary>
    public class DummyLookupStrategy : ILookupStrategy
    {
        public string Lookup(string value)
        {
            switch (value)
            {
                case "0107c3c5c4":
                    {
                        return "Plugins.CareBedDriver.zip";
                    }
                case "01068e105f":
                    {
                        return "Plugins.Contextua11073HubDriver.zip";
                    }
                default:
                    {
                        return "";
                    }
            }
        }
    }
}